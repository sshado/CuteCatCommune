using Microsoft.EntityFrameworkCore;

namespace Nyanplace.Server.Data
{
    public class NyanDbContext : DbContext
    {
        public NyanDbContext(DbContextOptions<NyanDbContext> options)
            : base(options)
        {
        }
        
        public DbSet<Nyanplace.Server.Models.Cuties> Todo { get; set; }
    }
}